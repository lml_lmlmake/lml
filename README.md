# LML
The Lua Mega Multi-purpose ... Meta Markup Language.

LML is a Lua library that allows creating Lua files that mimic aspects
of the behavior of Qt's QML. LML does not need its own parser, it
simply uses the syntactic sugar of Lua function calls with a single
table argument to mimic typed objects like in QML.

LML also implements one of the core features of QML: property
bindings. However, while QML makes use of Qt's signals and slots to
tell the source of a binding that the value of the target has changed
(i.e., the target *pushes* the information), in LML currently sources
*pull* the information from the target, i.e., recompute them on each
use. For GUI purposes this might kill the major advantages of bindings
but for config and make files (main domain of LML) each value is used
only a few times and thus the implementation does not matter that
much. However, implementing a *push* mechanism like QML is a 
possibility in the future, so do not rely on the current
implementation.

Example QML and LML
----
    //QML:
    Object {
        property string text : "Some Text."
        property string childtext : ch.text
        Object {
            id: ch
            property string text : "Some other Text."
        }
    }
    
    --LML
    return Object {
        text = "Some Text.",
        childtext = bind("ch").text,
        --with autobind: childtext = ch.text
        Object {
            id = "child"--with autobind: id = child,
            text = Some other Text."
        }
    }
    
What means "autobinds"?
----
LML files are loaded with a given environment. If `lml.autobind(env)`
is executed on this environment `env`, then a `__newindex` method
is added to the metatable of `env` that turns every undefined
identifier into a bond.
This looks nice and even closer to QML, but it is very hard to debug
as bonds appear whenever you forget to define a variable or make a
typo. Thus I do not recommend it in most cases.


License
----
 * This software is licensed under GPLv3+, see each file header.
 * If you need a more permissive license (e.g. BSD-style),
 please contact the author.

TODO
----
 * further testing
 * document API

