return Object {
    id = ptout,
    name = alias(pti1.name),
    bdname = ptout.name .. "1",
    n = 1,
    nbd = ptout.n + pti1.n + pti2.n,
    copyOI (Object) {id=pti1,n=1,name="old name"},
    copyOI (Object) {id=pti2,n=1},
}
