--[[
    Copyright (C) 2019   Frank Fuhlbrück <frank@fuhlbrueck.net>
  
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  
]]

local lml = {}

local CONST = {}
setmetatable(CONST,{
    CONST=true,
    __tostring=function() return "CONST"end
})
local const = function(c)
    return setmetatable({},{[CONST]=true,
        __tostring=function() return c end})
end
isconst = function(x)
    local mt = getmetatable(x)
    return mt and mt[CONST]
end

--These are *keys* used by the lml module, i.e.,
--they are used like:
--{[IDTABLE] = {myid = {1,2,3} }}
--ID tables are ordinary table with ids as keys
--Bind tables contain elements of the form {obj=,key=,bind=}
--where a obj[key] contains the 
--(yet unfulfilled) bind.
local IDTABLE = const("IDTABLE")
local INNERIDTABLE = const("INNERIDTABLE")--see
local NAMESPACE = const("NAMESPACE")
local OPENNAMESPACE = const("OPENNAMESPACE")
local BINDTABLE = const("BINDTABLE")
local INNERBINDTABLE = const("INNERBINDTABLE")
local ALIASTABLE = const("ALIASTABLE")
local OUTERID = const("OUTERID")--most ids become outer ids
local INNERIDS = const("INNERIDS")--top elements of bind scopes have an inner id
-- local INNERCHILD = const("INNERCHILD")
local PROTO = const("PROTO")
local PROTONAME = const("PROTONAME")
--Usually values in instantiated prototypes are 
--more like copy-on-write, because
--of Lua's use of __index.
--But for some objects (like GUI elements) you might want a fresh
--copy already on instantiation.
local COPYONINST = const("COPYONINST")
local ALIAS = const("ALIAS")
local EXP = const("EXP")
local EVAL = const("EVAL")
local LMLNR = const("LMLNR")
local INIFUN = const("INIFUN")

--needed by newFromTable, so defined early
local isAlias = function(e1)
    return (type(e1)=="table" and rawget(e1,ALIAS))
end
local isOneAlias = function(e1,e2)
    return (type(e1)=="table" and rawget(e1,ALIAS)) or
        (type(e2)=="table" and rawget(e2,ALIAS))
end

local function copyAsNeeded(obj,repltab,nottoplvl)
    local ret = setmetatable({},getmetatable(obj))
    rawset(ret,PROTO,rawget(obj,PROTO))
    rawset(ret,COPYONINST,true)
--     rawset(ret,INNERCHILD,rawget(obj,INNERCHILD))
    lml.object.n = lml.object.n + 1
    rawset(ret,LMLNR,lml.object.n)
    
    repltab = repltab or {}
    repltab[obj] = ret

    for k,v in pairs(obj) do
        if  type(v) == "table" and not isconst(k) and
                rawget(v,COPYONINST) then
            rawset(ret,k,copyAsNeeded(v,repltab,true))
        elseif nottoplvl  and not isconst(k) then
            rawset(ret,k,v)
        end
    end
    if rawget(obj,INNERBINDTABLE) or rawget(obj,INNERIDTABLE) then--bind scope root
--         repltab = {[obj]=ret}
--         
--         for k,v in pairs(obj) do
--             if  type(v) == "table" and (not isconst(k)) and
--                 rawget(v,COPYONINST) and rawget(obj,INNERCHILD)[k] 
--                     then
--                 rawset(ret,k,copyAsNeeded(v,repltab,true))
--             end
--         end
        
--         print("------\nrepl",obj,ret)
--         for k,v in pairs(repltab) do
--             print("",k,"->",v)
--         end
        oidtab = rawget(obj,INNERIDTABLE) or {}
        assert(oidtab,"Error: bind root without INNERIDTABLE")
        nidtab = {}
        for ns,subtab in pairs(oidtab) do
            local nsubtab = {}
            nidtab[ns] = nsubtab
            for id,o in pairs(subtab) do
--                 print("",id,"->",o,repltab[o])
                assert(repltab[o],tostring(obj)..": "..tostring(o))
                nsubtab[id] = repltab[o]
            end
        end
        obdtab = rawget(obj,INNERBINDTABLE)
        if obdtab then
            nbdtab = {}
            for o,subtab in pairs(obdtab) do
                assert(repltab[o],tostring(obj)..": "..tostring(o))
                local nsubtab = {}
                nbdtab[repltab[o]] = nsubtab
                for key,bd in pairs(subtab) do
                    nsubtab[key] = bd
                end
            end
        else
            nbdtab = nil
        end
        
        rawset(ret,BINDTABLE,nbdtab)--does not contain static-static binds
        --of rawget(obj,INNERBINDTABLE)

        rawset(ret,INNERIDTABLE,nidtab)
        lml.refulfillBonds(ret)
    end
    
    return ret
end

lml.object = {
    n = 0,
    new = function(proto,init)
        if type(init) == "table" then
            return lml.object.newFromTable(proto,init)
        elseif type(init) == "string" then
            return lml.object.newFromString(proto,init)
        else
            error("lml.object initialised with neither table nor string")
        end
    end,
    
    newFromTable = function(proto,init)
        local obj
        if proto[COPYONINST] then--every proto is a bind scope root
            obj = copyAsNeeded(proto)
        else
            obj = setmetatable({},lml.object)
            lml.object.n = lml.object.n + 1
            rawset(obj,LMLNR,lml.object.n)
        end
        rawset(obj,PROTO,proto)
        local idtab,bdtab,ptl = {},{[obj]={}},#proto
        if init.id then
            --no "" required for IDs with autobind
            init.id = tostring(init.id)
            idtab[init.id] = obj
            rawset(obj,OUTERID,{[OPENNAMESPACE]=init.id})
            init.id = nil
        end

        for k,v in pairs(init) do
            if type(k)=="string" or type(k)=="number" then
                if type(v) ~= "table" then
                    goto continue
                end
                if getmetatable(v) == lml.bond then
                    bdtab[obj][k] = v
                    rawset(init,k,nil)
                    goto continue
                end
                if v[COPYONINST] then
                    rawset(obj,COPYONINST,true)
                end
                if rawget(v,IDTABLE) then
                    for id,o in pairs(rawget(v,IDTABLE)) do
                        if idtab[id] then
                            error("lml: id collision "..id)
                        end
                        idtab[id] = o
                    end
                    rawset(v,IDTABLE,nil)
                end
                if rawget(v,BINDTABLE) then
                    for o,subtab in pairs(rawget(v,BINDTABLE)) do
                        bdtab[o] = subtab
                    end
                    rawset(v,BINDTABLE,nil)
                end
                ::continue::
            end
        end
        if next(idtab) ~= nil then
            rawset(obj,IDTABLE,idtab)
        end
        --If the object has no bonds, remove its own entry in the
        --bindtable (this may result in an empty bdtab, s.b.).
        if not next(bdtab[obj]) then
            bdtab[obj] = nil
        end
        if next(bdtab) then
            rawset(obj,BINDTABLE,bdtab)
        end
        for k,v in pairs(init) do
            if type(k) == "number" then
                rawset(obj,k+ptl,v)
            elseif type(k) == "string" then
                --keep aliases intact, do not use rawset here
                obj[k]=v
                --overwriting bindings via init tables should
                --be possible, thus:
                if obj[INNERBINDTABLE] and obj[INNERBINDTABLE][obj]
                        and obj[INNERBINDTABLE][obj][k] and not
                        isAlias(obj[INNERBINDTABLE][obj][k])
                then
                    obj[INNERBINDTABLE][obj][k] = nil
                end
            end
        end

        if obj[INIFUN] then
            obj[obj[INIFUN]](obj)
        end
        
        return obj
    end,
    
    __index = function(obj,key)
--         if type(key)=="table" or type(key)=="number" or
--             type(key)=="string" then
            return (rawget(obj,ALIASTABLE) and 
                    rawget(obj,ALIASTABLE)[key]) or 
                rawget(obj,PROTO)[key]
--         end
    end,
    
    __newindex = function(obj,key,val)
        local tgt = (rawget(obj,ALIASTABLE) and 
                    rawget(obj,ALIASTABLE)[key]) or
                    (rawget(obj,PROTO)
                    and rawget(obj,PROTO)[key])
        if type(tgt)=="table" and 
                type(rawget(tgt,ALIAS)) == "function" then
            rawget(tgt,ALIAS)(val)
        else
            rawset(obj,key,val)
        end
    end,
    
    __tostring = function(obj)
        return (obj[PROTONAME] or "Unknown")
            .. "("..rawget(obj,LMLNR)..")"
    end
}
lml.object.__call = lml.object.new
lml.Object = lml.object.new(PROTO,{})
lml.Object[PROTONAME] = "Object"

lml.proto = function(obj)
    assert(getmetatable(obj)==lml.object,
           "Error: proto(...) called on non-LML object.")
    return rawget(obj,PROTO)
end

lml.protoname = function(obj)
    assert(getmetatable(obj)==lml.object,
           "Error: protoname(...) called on non-LML object.")
    return obj[PROTONAME]
end

lml.copyOI = function(proto)
    return function(init)
        local obj = proto(init)
        rawset(obj,COPYONINST,true)
        return obj
    end
end

lml.shareOI = function(proto)
    return function(init)
        local obj = proto(init)
        rawset(obj,COPYONINST,nil)
        return obj
    end
end

lml.iniFun = function(proto,fun)
    fun = fun or "init"
    return function(init)
        local obj = proto(init)
        rawset(obj,INIFUN,fun)
        obj[fun](obj)
        return obj
    end
end

--forward declaration,upval for ffbind... functions
local ffbind = {}
lml.plain = function(val)
    return (getmetatable(val)==ffbind and val()) or val
end
local exargsStr = function(exargs)
    local s = ""
    for _,v in ipairs(exargs) do
        local e = (type(v)=="string" and ('([==['..v..']==])')) or v
        s = s .. "," .. e
    end
    return s
end
local bindExprJoin = function(op,postop)
    local lb = (postop and "") or "("
    local rb = (postop and "") or ")"
    postop = postop or ""
    return function(e1,e2,...)
        local exargs = {...}
        postop = ((#exargs > 0 and exargsStr(exargs)) or "")..postop
        e1 = (type(e1)=="string" and ('([==['..e1..']==])')) or e1
        e2 = (type(e2)=="string" and ('([==['..e2..']==])')) or e2
        return lml.bond.new(
            lb..tostring(e1)..op..tostring(e2)..postop..rb,
            isOneAlias(e1,e2)
        )
    end
end
local ffbindInsertVal = function(op)
    local opfun = load("return function(a,b) return a "..   
            op.." b end")()
    return function(e1,e2)
        e1 = (getmetatable(e1)==ffbind and e1[EVAL]()) or e1
        e2 = (getmetatable(e2)==ffbind and e2[EVAL]()) or e2
        return opfun(e1,e2)
    end
end
local bindUnExprJoin = function(op)
    return function(e1)
        return lml.bond.new(
            "("..op..tostring(e1)..")",isAlias(e1)
        )
    end
end
local ffbindUnInsertVal = function(op)
    local opfun =
            load("return function(a) return "..op.." a end")()
    return function(e1)
        return opfun(e1[EVAL]())
    end
end

--fulfilled bonds:
ffbind.__tostring = function(ff) return tostring(rawget(ff,EVAL)()) end
ffbind.__call = function(ff,...) return rawget(ff,EVAL)(...) end
ffbind.__index = function(ff,idx)
    if idx == COPYONINST then
        return nil
    end
    local ret = rawget(ff,EVAL)()[idx]
    if type(ret) == "function" then
        return function(self,...)
            if getmetatable(self) == ffbind then
                self = rawget(self,EVAL)()
            end
            return ret(self,...)
        end
    end
    return ret
end
ffbind.__add = ffbindInsertVal("+")
ffbind.__sub = ffbindInsertVal("-")
ffbind.__mul = ffbindInsertVal("*")
ffbind.__div = ffbindInsertVal("/")
ffbind.__mod = ffbindInsertVal("%")
ffbind.__pow = ffbindInsertVal("^")
ffbind.__unm = ffbindUnInsertVal("-")
ffbind.__idiv = ffbindInsertVal("//")
ffbind.__band = ffbindInsertVal("&")
ffbind.__bor = ffbindInsertVal("|")
ffbind.__bxor = ffbindInsertVal("~")
ffbind.__bnot = ffbindUnInsertVal("~")
ffbind.__shl = ffbindInsertVal("<<")
ffbind.__shr = ffbindInsertVal(">>")
ffbind.__bxor = ffbindInsertVal("~")
ffbind.__concat = ffbindInsertVal("..")
ffbind.__len = ffbindUnInsertVal("#")
ffbind.__eq = ffbindInsertVal("==")
ffbind.__lt = ffbindInsertVal("<")
ffbind.__le = ffbindInsertVal("<=")

lml.bond = {
    new = function(exp,alias)
        local bd = {[EXP]=exp,[ALIAS]=alias or false,
            [NAMESPACE]=OPENNAMESPACE}
        setmetatable(bd,lml.bond)
        return bd
    end,
    __tostring = function(bd) return rawget(bd,EXP) end,
    __index = bindExprJoin("[","]"),
    __call = bindExprJoin("(",")"),
    __add = bindExprJoin("+"),
    __sub = bindExprJoin("-"),
    __mul = bindExprJoin("*"),
    __div = bindExprJoin("/"),
    __mod = bindExprJoin("%"),
    __pow = bindExprJoin("^"),
    __unm = bindUnExprJoin("-"),
    __idiv = bindExprJoin("//"),
    __band = bindExprJoin("&"),
    __bor = bindExprJoin("|"),
    __bxor = bindExprJoin("~"),
    __bnot = bindUnExprJoin("~"),
    __shl = bindExprJoin("<<"),
    __shr = bindExprJoin(">>"),
    __bxor = bindExprJoin("~"),
    __concat = bindExprJoin(".."),
    __len = bindUnExprJoin("#"),
    __eq = bindExprJoin("=="),
    __lt = bindExprJoin("<"),
    __le = bindExprJoin("<="),
}
lml.bind = lml.bond.new
lml.alias = function(exp)
    --tostring enables aliases without "" when autobind is active
    local bd = lml.bond.new(tostring(exp),true)
    return bd
end

lml.fulfillBond = function(bd,env)
-- -- useful for debugging rebinding 
--     print("-------")
--     print(rawget(bd,EXP))
--     for k,v in pairs(env) do
--         print(k,v)
--     end
    local eval,err = load("return "..rawget(bd,EXP),nil,nil,env)
    assert(not err,err)
    local alias
    if rawget(bd,ALIAS) then
        alias,err = load(rawget(bd,EXP).."= ...",nil,nil,env)
        assert(not err,err)
    end
    return setmetatable({
        [EXP] = rawget(bd,EXP),
        [EVAL] = eval,
        [ALIAS] = alias
    },ffbind)
end

lml.fulfillBonds = function(obj,outerId)
    local idtab,bdtab = rawget(obj,IDTABLE) or {},
        rawget(obj,BINDTABLE) or {}
    local ibdtab = rawget(obj,INNERBINDTABLE) or {}
    for o,subtab in pairs(bdtab) do
        ibdtab[o] = ibdtab[o] or {}
        for key,bd in pairs(subtab) do
            ibdtab[o][key] = bd
            rawset(bd,NAMESPACE,obj)
            if rawget(bd,ALIAS) then
                rawset(o,ALIASTABLE,rawget(o,ALIASTABLE) or {})
                rawset(o,key,nil)
                rawset(rawget(o,ALIASTABLE),key,
                    lml.fulfillBond(bd,idtab))
            else
                rawset(o,key,lml.fulfillBond(bd,idtab))
            end
        end
    end
    
--     rawset(obj,INNERCHILD,{})
    for id,o in pairs(idtab) do
        rawset(o,OUTERID,{[obj]=id})
    end
    rawset(obj,INNERIDS,rawget(obj,INNERIDS) or {})
    rawget(obj,INNERIDS)[obj] = (rawget(obj,OUTERID) and 
        rawget(obj,OUTERID)[obj])
    rawset(obj,OUTERID,(outerId and {[OPENNAMESPACE]=outerId}) or nil)
    if rawget(obj,BINDTABLE) then
        rawset(obj,INNERBINDTABLE,ibdtab)
        rawset(obj,BINDTABLE,nil)
    end
    if rawget(obj,IDTABLE) then
        rawset(obj,INNERIDTABLE,rawget(obj,INNERIDTABLE) or {})
        rawget(obj,INNERIDTABLE)[obj] = rawget(obj,IDTABLE)
--         for k,v in pairs(obj) do
--             if not isconst(k) then
--                 rawget(obj,INNERCHILD)[k] = true
--             end
--         end
    end
    rawset(obj,IDTABLE,(outerId and {[outerId]=obj}) or nil)

end

lml.refulfillBonds = function(obj)
    local bdtab = rawget(obj,BINDTABLE) or {}
    local iidtab = rawget(obj,INNERIDTABLE) or {}
    local ibdtab = rawget(obj,INNERBINDTABLE) or {}
    for o,subtab in pairs(bdtab) do
        ibdtab[o] = ibdtab[o] or {}
        for key,bd in pairs(subtab) do
            ibdtab[o][key] = bd
            local env = iidtab[rawget(bd,NAMESPACE)]
            if rawget(bd,ALIAS) then
                rawset(o,ALIASTABLE,rawget(o,ALIASTABLE) or {})
                rawset(o,key,nil)
                rawset(rawget(o,ALIASTABLE),key,lml.fulfillBond(bd,env))
            else
                rawset(o,key,lml.fulfillBond(bd,env))
            end
        end
    end
    rawset(obj,INNERBINDTABLE,ibdtab)
    rawset(obj,BINDTABLE,nil)
--     rawset(obj,INNERCHILD,{})
--     for k,v in pairs(obj) do
--         if not isconst(k) then
--             rawget(obj,INNERCHILD)[k] = true
--         end
--     end
end

lml.bondScope = function(proto,outerId)
    return function(init)
        local obj = proto(init)
        lml.fulfillBonds(obj,outerId)
        return obj
    end
end

lml.autobind = function(env)
    local mt = getmetatable(env) or {}
    setmetatable(env,mt)
    mt.__index = function(_,id) 
        if type(id) == "string" then
            return lml.bond.new(id)
        end
    end
end

local includeDirRel = function(pathpref)
    return function(path,env,keepPathpref)
        local iDR,lLFR = env.includeDirRel,env.loadLmlFileRel
        env = lml.includeDir(pathpref..path,env)
        if keepPathpref then
            env.includeDirRel,env.loadLmlFileRel = iDR,lLFR
        end
        return env
    end
end

local loadLmlFileRel = function(pathpref)
    return function(path,env)
        return lml.includeDir(pathpref..path,env)
    end
end

lml.lmlInsert = function(env,file)
    for k,v in pairs(lml) do
        env[k] = v
    end
    file = file or ""
    local pathpref = file:match(".*/") or "./"
    env.includeDirRel = includeDirRel(pathpref)
    env.loadLmlFileRel = loadLmlFileRel(pathpref)
end

--env must be freely usable by this function,
--in most cases you should not pass _G or _ENV
lml.loadLmlFile = function(file,env,autobind,protoname)
    env = env or {}
    lml.lmlInsert(env,file)
    if autobind then
        lml.autobind(env)
    end
    local obj, err = loadfile(file,nil,env)
    if err then
        error(err)
    end
    obj = obj()
    assert(obj,"Lmlfile "..file.."does not return a value.")
    lml.fulfillBonds(obj)
    rawset(obj,PROTONAME,protoname)
    return obj
end

--env must be freely usable by this function,
--in most cases you should not pass _G or _ENV
lml.loadLmlString = function(str,env,autobind,protoname)
    env = env or {}
    lml.lmlInsert(env)
    if autobind then
        lml.autobind(env)
    end
    local obj, err = load(str,nil,nil,env)
    if err then
        error(err)
    end
    obj = obj()
    assert(obj,"Lmlstring [["..str.."]] does not return a value.")
    lml.fulfillBonds(obj)
    rawset(obj,PROTONAME,protoname)
    return obj
end



--Objects only to be loaded when instantiated.
--The table registries contains tables where the loadLater
--will be replaced on loading.
lml.loadLater = {
    new = function(name,path,registries)
        ll = setmetatable({
            name = name,
            path = path,
            registries = registries               
        },lml.loadLater)
        return ll
    end,
    
    __call = function(ll,...)
        local name,path,registries = ll.name,ll.path,ll.registries
        local env = (registries and registries[1]) or nil
        local obj
        if env then
            obj = env[name]
            if obj ~= nil and getmetatable(obj) ~= lml.loadLater then
                return obj(...)--already loaded
            end
        end
        obj = lml.loadLmlFile(path,env,false,name)
        for _,reg in pairs(registries) do
            reg[name] = obj
        end
        return obj(...)
    end
}

local ls = function(path)
    local success,lsit = pcall(io.lines,path.."/.ls")
    if success then
        return function(file) return file:sub(-4) == ".lua" end,lsit
    end
    
    local lfs = require "lfs"
    return function(file)
        return lfs.attributes(path.."/"..file,"mode") == "file"
                and file:sub(-4) == ".lua"
        end, lfs.dir(path)
end

--env must be freely usable by this function,
--in most cases you should not pass _G or _ENV
lml.includeDir = function(path,env)
    env = env or {}
    local nameList = {}
    local islua,lsit,lsinit = ls(path)
    for file in lsit,lsinit do
        if islua(file) then
            local name = file:sub(1,-5)
            env[name] = lml.loadLater.new(name,path.."/"..file,{env})
            table.insert(nameList,name)
        end    
    end
    for _,name in ipairs(nameList) do
        local ll = env[name]
        if getmetatable(ll) == lml.loadLater then
            env[name] = 
                lml.loadLmlFile(path.."/"..ll.name..".lua",
                                env,false,name)
        end
        --else: already loaded, nothing to do
    end
    return env
end



return lml




