--[[
    Copyright (C) 2019   Frank Fuhlbrück <frank@fuhlbrueck.net>
  
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
  
]]

local lml = require "lml"

-- local obj = lml.lmlLoadFile("TestDir/TestObject.lua")

local env = {}
lml.includeDir("TestDir/",env)

local obj = env.TestObject
print(obj.nbd,obj[1].nbd,obj[1].n,obj[1][1].n,obj[1][2].n)
print(obj.name,obj[2].name,obj[2][1].name)
obj.name = "newer name"
print(obj.name,obj[2].name,obj[2][1].name)
obj[2][1].name = "brand new name"
print(obj.name,obj[2].name,obj[2][1].name)


-- for k,v in pairs(obj[2]) do
--     print(k,v)
-- end
